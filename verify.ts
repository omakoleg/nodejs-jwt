import { readFileSync } from 'fs';
import { join } from 'path';
import * as  jwt from 'jsonwebtoken';
import { issuer } from './config';

const token = readFileSync(join('keys', 'token.txt')).toString();
const publicKey = readFileSync(join('keys', '.public.key.pem'));

const decodedToken = jwt.verify(token, publicKey, {
    issuer,
    algorithms: ['RS256'],
    maxAge: '1 day'
});

console.log(`Decoded token data`, decodedToken);
