# nodejs-jwt

Typescript samples for asymmetric keys generation, JWT signing and validation using JWKS API.

# Scripts Included

1) `yarn gen` - Generate asymmetric Keys pair using Node.js `crypto` module. 

Private key saved to `./keys/.private.key`

Public key saved to `./keys/.public.key.pem`

![image](./docs/1.png)

2) `yarn sign` - Loads Private Key (`./keys/.private.key`) and sign data payload:

```ts
{
    userId: 'a-b-c-d'
}
```
Then save it into `./keys/token.txt`.

Value could be inserted into [jwt.io online editor](https://jwt.io/) to see details what is inside.

![image](./docs/2.png)

3) `yarn verify` - Load JWT token (`./keys/token.txt`) and verify it was signed with Public Key (`./keys/.public.key.pem`)

![image](./docs/3.png)

4) 
4.A) `yarn jwks-server` - Authentication API server exposing `http://localhost:3344/keys` json JWKS endpoint.
Which internally use `./keys/.public.key.pem`

4.B) `yarn jwks-client-server` - Api client exposing API endpoint `http://localhost:2233/with-auth`
which internally loads JWKS key from `http://localhost:3344/keys` and verify provided Bearer token in `Authentication` Header.

4.C) `yarn make-request` - Make sample request to `jwks-client-server`(`http://localhost:2233/with-auth`) and provide 
authentication header from `./keys/token.txt`

![image](./docs/4.png)

5) `yarn clean` - Remove all `*.js` file sin directory.

