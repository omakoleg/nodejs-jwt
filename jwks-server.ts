import express from 'express';
import { readFile } from 'fs';
import { join } from 'path';
import { kid } from './config';

// not typed
var pem2jwk = require('pem-jwk').pem2jwk;

const app = express()

app.get('/keys', (req, res) => {
    readFile(join('keys', '.public.key.pem'), (err, pem) => {
        if (err) { return res.status(500); }
        const jwk = pem2jwk(pem);
        res.json({
            keys: [{
                ...jwk,
                kid,
                use: 'sig'
            }]
        });
    });
})

app.listen(3344, () => console.log(`JWKS API is on port 3344`))