import express, { Request, Response } from 'express';
import { issuer } from './config';
import jwt from 'express-jwt';
import jwksRsa from 'jwks-rsa';

const checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `http://localhost:3344/keys`
    }),
    issuer,
    algorithms: ['RS256'],
    requestProperty: 'tokenData'
});

const app = express()
app.get('/with-auth', checkJwt, (req: Request, res: Response) => {
    res.send(`Result: ${JSON.stringify(req.tokenData)}`);
})

app.listen(2233, () => console.log(`Backend is running on port 2233`))