import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import * as  jwt from 'jsonwebtoken';
import { passphrase, issuer, kid, JwtDataPayload } from './config';

const payload: JwtDataPayload = {
    userId: 'a-b-c-d'
}
const privateKey = readFileSync(join('keys', '.private.key'));
const token = jwt.sign(payload, {
    key: privateKey,
    passphrase
}, {
    algorithm: 'RS256',
    expiresIn: '1 day',
    issuer,
    keyid: kid
});

writeFileSync(join('keys', 'token.txt'), token);

console.log(`Signed token: ${token}`);
