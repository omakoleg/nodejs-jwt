export const passphrase = 'some-secret-phrase';
export const kid = 'my-key-id';
export const issuer = 'my-issuer-name';

export interface JwtDataPayload {
    userId: string
}

// required for jwks-client-server.ts 
// to extend express.Request type with jwt decoded payload
declare module 'express' {
    interface Request {
        tokenData?: JwtDataPayload
    }
}