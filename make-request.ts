import request from "request";
import { readFileSync } from 'fs';
import { join } from 'path';

const token = readFileSync(join('keys', 'token.txt')).toString();

var options = {
    method: 'GET',
    url: 'http://localhost:2233/with-auth',
    headers: { authorization: `Bearer ${token}` }
};

request(options, function (error, response, body) {
    if (error) throw new Error(error);
    console.log(body);
});
